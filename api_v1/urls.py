from rest_framework import routers
from .views.announcements import AnnouncementViewSet
from .views.accounts import RegisterView, UserListView
from .views.collectioncentres import (CollectionCentreList, CollectionCentreDetails,
UnitList, UnitDetail, ItemCategoryList, ItemCategoryDetail, ItemNameList, ItemNameDetail,
InventorySummaryListView, InventorySummaryDetailsView)
from .views.accounts import UserDetailView
# from .views.camps import CampViewSet
from django.urls import path
from django.conf.urls import url
from api_v1 import urls as api_v1_urls

urlpatterns = []

router = routers.SimpleRouter()
router.register('announcements', AnnouncementViewSet, 'announcements')
urlpatterns += [path('register/', RegisterView.as_view())]
urlpatterns += [path('userlist/', UserListView.as_view())]
urlpatterns += [path('userlist/<int:pk>/', UserDetailView.as_view()), ]
# router.register('camps', CampViewSet, 'camps')
urlpatterns += router.urls

urlpatterns += [path('collectioncentre/', CollectionCentreList.as_view())]
# urlpatterns += [path('collectioncentre/<int:pk>', CollectionCentreDetails.as_view())]
urlpatterns += [path('unit/', UnitList.as_view())]
urlpatterns += [path('unit/<int:pk>', UnitDetail.as_view())]
urlpatterns += [path('item/category/', ItemCategoryList.as_view())]
urlpatterns += [path('item/category/<int:pk>', ItemCategoryDetail.as_view())]
urlpatterns += [path('item/name/', ItemNameList.as_view())]
urlpatterns += [path('item/name/<int:pk>', ItemNameDetail.as_view())]
urlpatterns += [path('collectioncentre/<int:pk>', CollectionCentreDetails.as_view())]
urlpatterns += [path('collectioncentre/<int:pk>/inventory_summary/<int:inventory_id>', InventorySummaryDetailsView.as_view())]



