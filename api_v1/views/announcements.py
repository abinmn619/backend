from rest_framework import status, viewsets
from announcements.serializers import AnnouncementSerializer
from announcements.models import Announcement
from rest_framework.response import Response

class AnnouncementViewSet(viewsets.ViewSet):

    def list(self, request, **kwargs):
        announcements = Announcement.objects.filter(is_active=True)
        serializer = AnnouncementSerializer(announcements, many=True)
        response = serializer.data
        return Response(response, status=status.HTTP_200_OK)