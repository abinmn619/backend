from django.http import Http404
from rest_framework.views import APIView
from rest_framework import status
from collectioncentres.models import CollectionCentre, Unit, ItemCategory, ItemName, CollectionCentreInventorySummary
from collectioncentres.serializers import (CollectionCentreSerializer,
                                           UnitSerializer, ItemCategorySerializer, ItemNameSerializer,
                                           CollectionCentreInventorySummarySerializer)

from rest_framework.response import Response
from django.http import Http404


class CollectionCentreList(APIView):
    def get(self, request, format=None):
        collection_centres = CollectionCentre.objects.all()
        serializer = CollectionCentreSerializer(collection_centres, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CollectionCentreSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UnitList(APIView):
    def get(self, request, format=None):
        units = Unit.objects.all()
        serializer = UnitSerializer(units, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UnitSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UnitDetail(APIView):
    def get_object(self, pk):
        try:
            return Unit.objects.get(pk=pk)
        except Unit.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        unit = self.get_object(pk)
        serializer = UnitSerializer(unit)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        unit = self.get_object(pk)
        serializer = UnitSerializer(unit, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        unit = self.get_object(pk)
        unit.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ItemCategoryList(APIView):
    def get(self, request, format=None):
        item_categories = ItemCategory.objects.all()
        serializer = ItemCategorySerializer(item_categories, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ItemCategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemCategoryDetail(APIView):
    def get_object(self, pk):
        try:
            return ItemCategory.objects.get(pk=pk)
        except ItemCategory.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        item_category = self.get_object(pk)
        serializer = ItemCategorySerializer(item_category)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        item_category = self.get_object(pk)
        serializer = ItemCategorySerializer(item_category, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CollectionCentreDetails(APIView):
    def get_object(self, pk):
        try:
            return CollectionCentre.objects.get(pk=pk)
        except CollectionCentre.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        collection_center = self.get_object(pk)
        serializer = CollectionCentreSerializer(collection_center)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        collection_center = self.get_object(pk)
        serializer = CollectionCentreSerializer(collection_center, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        item_category = self.get_object(pk)
        item_category.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ItemNameList(APIView):
    def get(self, request, format=None):
        item_names = ItemCategory.objects.all()
        serializer = ItemCategorySerializer(item_names, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ItemCategorySerializer(data=request.data)
        collection_center = self.get_object(pk)
        collection_center.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InventorySummaryListView(APIView):

    def get(self, request, pk, format=None):
        collection_centre = CollectionCentre.objects.get(pk=pk)
        inventory_summary = collection_centre.  collectioncentreinventorysummary_set.all()
        serializer = CollectionCentreInventorySummarySerializer(inventory_summary, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CollectionCentreInventorySummarySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemNameDetail(APIView):
    def get_object(self, pk):
        try:
            return ItemCategory.objects.get(pk=pk)
        except ItemCategory.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        item_name = self.get_object(pk)
        serializer = ItemNameSerializer(item_name)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        item_name = self.get_object(pk)
        serializer = ItemNameSerializer(item_name, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        item_name = self.get_object(pk)
        item_name.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class InventorySummaryDetailsView(APIView):
    def get_object(self, pk, inventory_id):
        try:
            inventory_summary = CollectionCentreInventorySummary.objects.get(collection_centre=pk, id=inventory_id)
            return  inventory_summary
        except CollectionCentreInventorySummary.DoesNotExist:
            raise Http404

    def get(self, request, pk, inventory_id, format=None):
        inventory_summary = self.get_object(pk, inventory_id)
        serializer = CollectionCentreInventorySummarySerializer(inventory_summary)
        return Response(serializer.data)

    def put(self, request, pk, inventory_id, format=None):
        inventory_summary = self.get_object(pk)
        serializer = CollectionCentreInventorySummarySerializer(inventory_summary, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        inventory_summary = self.get_object(pk)
        inventory_summary.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
