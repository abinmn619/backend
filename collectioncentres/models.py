from django.db import models
from user_accounts.models import CustomUser


class CollectionCentre(models.Model):
    latitude = models.CharField(
        max_length=20,
        help_text='Latitude',
    )
    longitude = models.CharField(
        max_length=20,
        help_text='Longitude',
    )
    name = models.CharField(
        max_length=200,
        help_text='Collection Centre name',
    )
    address = models.CharField(
        max_length=500,
        help_text='Collection Centre Address',
    )
    manager = models.CharField(
        max_length=200,
        help_text='Collection Centre Manager'
    )
    modified_by = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        help_text='Collection Centre creator'
    )

    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Unit(models.Model):
    name = models.CharField(
        max_length=20,
        help_text='Unit of measurement'
    )

    def __str__(self):
        return self.name


class ItemCategory(models.Model):
    name = models.CharField(
        max_length=50,
    )

    def __str__(self):
        return self.name


class ItemName(models.Model):
    name = models.CharField(
        max_length=100,
        help_text='Item Name'
    )

    def __str__(self):
        return self.name


class CollectionCentreInventorySummary(models.Model):
    collection_centre = models.ForeignKey(
        CollectionCentre,
        on_delete=models.CASCADE,
    )
    item = models.ForeignKey(
        ItemName,
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ItemCategory,
        on_delete=models.CASCADE,
    )
    unit = models.ForeignKey(
        Unit,
        on_delete=models.CASCADE,
    )
    quantity = models.IntegerField(
        default=0,
        help_text='Quantity in stock'
    )
    required_quantity = models.IntegerField(
        default=0,
        help_text='Quantity required'
    )

    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.item.name


class InventoryLog(models.Model):
    collection_centre = models.ForeignKey(
        CollectionCentre,
        on_delete=models.CASCADE,
    )
    item = models.ForeignKey(
        ItemName,
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ItemCategory,
        on_delete=models.CASCADE,
    )
    unit = models.ForeignKey(
        Unit,
        on_delete=models.CASCADE,
    )
    quantity = models.IntegerField(
        help_text='Quantity in stock'
    )
    donor_name = models.CharField(
        max_length=200,
    )
    donor_address = models.CharField(
        max_length=500,
    )
    added_by = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        help_text='Collection Centre creator'
    )
    added_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s-%s' % (self.donor_name, self.item.name)
